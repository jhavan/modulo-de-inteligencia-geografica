require([
    'esri/config',
    'esri/Map',
    'esri/WebMap',
    'esri/views/MapView',
    'esri/layers/FeatureLayer',
    'esri/widgets/Legend',
    'esri/widgets/Expand',
    'esri/widgets/Locate',
    "esri/widgets/Measurement",
    "esri/widgets/BasemapGallery",
    "esri/Graphic",
    "esri/layers/GraphicsLayer",
    "esri/widgets/Sketch",
    "esri/widgets/Editor",
    "esri/geometry/geometryEngine"

], (
    esriConfig,
    Map,
    WebMap,
    MapView,
    FeatureLayer,
    Legend,
    Expand,
    Locate,
    Measurement,
    BasemapGallery,
    Graphic,
    GraphicsLayer,
    Sketch,
    Editor,
    geometryEngine
) => {
    esriConfig.apiKey = 'AAPK957e3ea85d1b4bb6b34add6bdca93b2fGURHvOWhGPxBj9QxKd6AVp7TagaO5PJpoCZ9xvNOTCBFGRo6NmmE8_PXZD06_ZNU';

    const map = new Map({
        basemap: 'arcgis-topographic'
    })

    //var print = document.getElementById('viewDiv');
    // html2pdf(print);

    const view = new MapView({
            map: map,
            center: [-48.8, -26.97],
            zoom: 12,
            container: 'viewDiv',
            constraints: {
                snapToZoom: false
            }
        })
        // Create new instance of the Measurement widget

    // Create the FeatureLayer using the popupTemplate
    const featureLayer = new FeatureLayer({
        url: 'https://services8.arcgis.com/pCELDSQrGaDG3Qun/ArcGIS/rest/services/Camada_teste_Modulo_de_Intelig%c3%aancia/FeatureServer/0',
        geometry: {
            type: "esriGeometryPoint"
        }


    })
    const featureLayer2 = new FeatureLayer({
        url: 'https://services8.arcgis.com/pCELDSQrGaDG3Qun/ArcGIS/rest/services/Camada_teste_Modulo_de_Intelig%c3%aancia/FeatureServer/1',
        geometry: {
            type: "esriGeometryPolyline"
        }
    })
    const featureLayer3 = new FeatureLayer({
        url: 'https://services8.arcgis.com/pCELDSQrGaDG3Qun/ArcGIS/rest/services/Camada_teste_Modulo_de_Intelig%c3%aancia/FeatureServer/2',
        geometry: {
            type: "esriGeometryPolygon"
        }
    })
    map.layers.push(featureLayer, featureLayer2, featureLayer3);

    view.when(() => {
        // get the first layer in the collection of operational layers in the WebMap
        // when the resources in the MapView have loaded.
        const featureLayer = map.layers.getItemAt(0);


        // legenda que expande

        const legend = new Expand({
            content: new Legend({
                view: view,
                layerInfos: [{
                    layer: featureLayer,
                    title: "Camadas Teste"
                }],
                style: "card" // other styles include 'classic'
            }),

            view: view,
            expanded: false
        });
        view.ui.add(legend, "top-left");



    });
    var Esributtoncontainer = document.querySelector(".esri-ui-top-left");

    // Widget Galeria de mapas base
    let basemapGallery = new BasemapGallery({
        view: view
    });
    view.ui.add(basemapGallery, {
        position: "bottom-left"
    });
    basemapGallery.visible = false;

    var mapButton = document.createElement("div");

    mapButton.classList.add("esri-component", "esri-icon-basemap", "esri-widget--button");
    mapButton.title = "Estilos de Mapas";
    Esributtoncontainer.appendChild(mapButton);
    mapButton.onclick = function() {
        if (basemapGallery.visible == true) {
            basemapGallery.visible = false;
            mapButton.classList.remove("active")
        } else {
            basemapGallery.visible = true
            mapButton.classList.add("active")
        }
    }



    // botão de localizaçao
    const locateBtn = new Locate({
        view: view
    });
    view.ui.add(locateBtn, {
        position: "top-left"
    });


    //Ferramenta de Medição
    const measurement = new Measurement({
        view: view,

    });
    view.ui.add(measurement, "top-right");
    measurement.visible = false;
    var measurementButton = document.createElement("div");
    measurementButton.classList.add("esri-component", "esri-icon-measure", "esri-widget--button");
    measurementButton.title = "Ferramenta de Medição";
    Esributtoncontainer.appendChild(measurementButton);
    measurementButton.onclick = function() {
        if (measurement.visible == true) {
            measurement.visible = false;
            measurementButton.classList.remove("active")
        } else {
            measurement.activeTool = "distance";
            measurement.visible = true
            measurementButton.classList.add("active")
        }
    }


    //Ferramenta de Sketch esri-icon-authorize

    /*const graphicsLayer = new GraphicsLayer();
    view.when(() => {
        const sketch = new Sketch({
            layer: graphicsLayer,
            view: view,
            // graphic will be selected as soon as it is created
            creationMode: "update"
        });

        view.ui.add(sketch, "top-right");
        sketch.visible = false;
        var sketchButton = document.createElement("div");
        sketchButton.classList.add("esri-component", "esri-icon-authorize", "esri-widget--button");
        sketchButton.title = "Ferramenta de Desenho";
        Esributtoncontainer.appendChild(sketchButton);
        sketchButton.onclick = function() {
            if (sketch.visible == true) {
                sketch.visible = false;
                sketchButton.classList.remove("active")
            } else {
                sketch.activeTool = "distance";
                sketch.visible = true
                sketchButton.classList.add("active")
            }
        }

    });*/

    // Ferramente e edição de camadas de feição


    const editor = new Editor({
        view: view,
        allowedWorkflows: ["create", "update"]
    });
    editor.visible = false;
    var editorButton = document.createElement("div");
    editorButton.classList.add("esri-component", "esri-icon-edit", "esri-widget--button");
    editorButton.title = "Ferramenta de Edição de camada"
    Esributtoncontainer.appendChild(editorButton);
    editorButton.onclick = function() {
        if (editor.visible == true) {
            editor.visible = false;
            editorButton.classList.remove("active")
        } else {

            editor.visible = true
            editorButton.classList.add("active")
        }
    }


    let poligonos = []
        //query features
    view.whenLayerView(featureLayer3, featureLayer2).then(function(layerView) {
        layerView.watch("updating", function(value) {
            if (!value) {
                // wait for the layer view to finish updating

                // query all the features available for drawing.
                layerView
                    .queryFeatures({
                        geometry: view.extent,
                        returnGeometry: true,
                        orderByFields: ["OBJECTID"]
                    })
                    .then(function(results) {
                        view.graphics.removeAll()
                        console.log(results.features)
                        for (let i = 0; i < results.features.length; i++) {

                            /* if (j == i) {
                                 if (j < results.features.length) {
                                     j++
                                 }

                             }*/
                            for (let j = 0; j < results.features.length; j++) {
                                if (i != j) {
                                    const intersecting = geometryEngine.intersect(results.features[i].geometry, results.features[j].geometry);
                                    //   }
                                    if (intersecting) {
                                        const intersectionGraphic = new Graphic({
                                            geometry: intersecting,
                                            symbol: {
                                                type: "simple-fill",

                                                color: "yellow",
                                                outline: {
                                                    color: "red"
                                                }
                                            }
                                        });
                                        view.graphics.add(intersectionGraphic);
                                        //for( j=0; j<resultes.featues.legnth; j++)   
                                    }
                                }
                            }
                        }



                        // console.log(poligonos.geometry.spatialReference.wkid)




                    })
                    .catch(function(error) {
                        console.error("query failed: ", error);
                    });
            }
        });
    });

    // Add the widget to the view
    view.ui.add(editor, "top-right");

    // Ferramenta para gerar pdf 
    var pdfButton = document.createElement("div");
    pdfButton.id = 'pdfButton';
    pdfButton.classList.add("esri-component", "esri-icon-documentation", "esri-widget--button");
    pdfButton.title = "Gerar PDF";


    Esributtoncontainer.appendChild(pdfButton);
    pdfButton.onclick = createPDF();

    let poligono = featureLayer3.geometryType
    let linha = featureLayer2.geometryType
    console.log(poligono, linha)






});




// add the toolbar for the measurement widgets
